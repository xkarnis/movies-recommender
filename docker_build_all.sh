#!/bin/bash
# TODO: move this to CI/CD pipeline
# Build docker images for all services

TAG=${1:-$(git branch --show-current)} # you can specify tag as first argument, otherwise current branch name is used

for SERVICE in {movie,account,genre,rating} ; do
    docker build -t quay.io/pa165/movie-recommender-"$SERVICE:$TAG" "$SERVICE/"
done
