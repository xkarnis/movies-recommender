package cz.muni.pa165.account.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.account.config.AccountTestConfiguration;
import cz.muni.pa165.account.facade.AccountFacade;
import cz.muni.pa165.core.api.account.AccountDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(AccountRestController.class)
@OverrideAutoConfiguration(enabled = true)
@Import(AccountTestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
public class AccountRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AccountFacade accountFacade;

    @Value("${rest.path.accounts}")
    private String restPath;

    @Test
    void findAllTest() throws Exception {
        mockMvc
            .perform(
                get(restPath)
                    .param("page", "0")
                    .param("size", "10")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void findByIdTest() throws Exception {
        mockMvc
            .perform(
                get(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void createTest() throws Exception {
        AccountDto accountDto = new AccountDto(1L, "Test", "Test", false);

        mockMvc
            .perform(
                post(restPath)
                    .content(objectMapper.writeValueAsString(accountDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isCreated());
    }

    @Test
    void updateTest() throws Exception {
        AccountDto accountDto = new AccountDto(1L, "Test", "Test", false);

        mockMvc
            .perform(
                put(restPath + "/1")
                    .content(objectMapper.writeValueAsString(accountDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void deleteTest() throws Exception {
        mockMvc
            .perform(
                delete(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNoContent());
    }
}
