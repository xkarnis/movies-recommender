package cz.muni.pa165.account.data.config;

import cz.muni.pa165.account.data.model.Account;
import cz.muni.pa165.account.data.repository.AccountRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AccountGenerator {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountGenerator(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @PostConstruct
    public void initAccounts() {
        accountRepository.saveAll(
            Set.of(
                new Account(
                    null,
                    "John Wick",
                    "johnlikespuppies@gmail.com",
                    true,
                    "xxx",
                    "sugar"
                ),
                new Account(
                    null,
                    "Habi Motter",
                    "secretvoldemort@gmail.com",
                    false,
                    "yyy",
                    "scar"
                ),
                new Account(
                    null,
                    "John Snow",
                    "winterbad@gmail.com",
                    false,
                    "zzz",
                    "ice"
                )
            )
        );
    }
}
