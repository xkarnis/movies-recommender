package cz.muni.pa165.account.facade;

import cz.muni.pa165.account.mappers.AccountMapper;
import cz.muni.pa165.account.service.AccountService;
import cz.muni.pa165.core.api.account.AccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountFacade {

    private final AccountService accountService;
    private final AccountMapper accountMapper;

    @Autowired
    public AccountFacade(
        AccountService accountService,
        AccountMapper accountMapper
    ) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
    }

    @Transactional(readOnly = true)
    public AccountDto findByEmail(String email) {
        return accountMapper.mapToDto(accountService.findByEmail(email));
    }

    @Transactional(readOnly = true)
    public AccountDto findById(Long id) {
        return accountMapper.mapToDto(accountService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<AccountDto> findAll(Pageable pageable) {
        return accountMapper.mapToPageDto(accountService.findAll(pageable));
    }

    @Transactional
    public AccountDto create(AccountDto account) {
        return accountMapper.mapToDto(
            accountService.create(accountMapper.mapToEntity(account))
        );
    }

    @Transactional
    public AccountDto update(Long id, AccountDto account) {
        account.setId(id);
        return accountMapper.mapToDto(
            accountService.update(accountMapper.mapToEntity(account))
        );
    }

    @Transactional
    public void deleteById(Long id) {
        accountService.delete(id);
    }
}
