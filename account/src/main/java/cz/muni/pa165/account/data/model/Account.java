package cz.muni.pa165.account.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String accountName;

    @Column(unique = true)
    private String email;

    // The rest of the attributes will most likely be deprecated because
    // we will use spring authentication instead.
    @Column
    private boolean isAdmin;

    @Column
    private String passwordHash;

    @Column
    private String passwordSalt;
}
