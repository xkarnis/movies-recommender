package cz.muni.pa165.account.service;

import cz.muni.pa165.account.data.model.Account;
import cz.muni.pa165.account.data.repository.AccountRepository;
import cz.muni.pa165.core.exception.ResourceNotFoundException;
import java.sql.SQLClientInfoException;
import java.sql.SQLDataException;
import java.sql.SQLException;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Transactional(readOnly = true)
    public Account findByEmail(String email) {
        return accountRepository
            .findByEmail(email)
            .orElseThrow(() ->
                new ResourceNotFoundException(
                    "Account with email: " + email + " was not found."
                )
            );
    }

    @Transactional(readOnly = true)
    public Account findById(Long id) {
        return accountRepository
            .findById(id)
            .orElseThrow(() ->
                new ResourceNotFoundException(
                    "Account with id: " + id + " was not found."
                )
            );
    }

    @Transactional(readOnly = true)
    public Page<Account> findAll(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    @Transactional
    public Account create(Account account) {
        try {
            return accountRepository.save(account);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                "Account with email: " + account.getEmail() + " already exists."
            );
        }
    }

    @Transactional
    public Account update(Account account) {
        findById(account.getId());
        return accountRepository.save(account);
    }

    @Transactional
    public void delete(Long id) {
        accountRepository.deleteById(id);
    }
}
