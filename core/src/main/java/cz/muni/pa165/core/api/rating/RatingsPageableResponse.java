package cz.muni.pa165.core.api.rating;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@JsonIgnoreProperties(ignoreUnknown = true, value = { "pageable" })
public class RatingsPageableResponse extends PageImpl<MovieRatingDto> {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public RatingsPageableResponse(
        @JsonProperty("content") List<MovieRatingDto> content,
        @JsonProperty("number") int number,
        @JsonProperty("size") int size,
        @JsonProperty("totalElements") Long totalElements
    ) {
        super(content, PageRequest.of(number, size), totalElements);
    }
}
