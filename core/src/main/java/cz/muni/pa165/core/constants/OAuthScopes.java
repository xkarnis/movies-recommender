package cz.muni.pa165.core.constants;

public class OAuthScopes {

    public static final String TEST_READ = "test_read";
    public static final String TEST_WRITE = "test_write";
}
