package cz.muni.pa165.core.api.genre;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Schema(title = "Movie genre", description = "Genre of a movie")
@Data
public class GenreDto {

    @Schema(
        description = "ID",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long id;

    @NotEmpty
    @Schema(description = "Name of the genre", example = "Action")
    private String name;
}
