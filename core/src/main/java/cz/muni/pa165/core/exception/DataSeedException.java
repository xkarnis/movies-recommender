package cz.muni.pa165.core.exception;

public class DataSeedException extends RuntimeException {

    private static final String msg = "could not load seeding data";

    public DataSeedException() {}

    public DataSeedException(String message) {
        super(message);
    }

    public DataSeedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataSeedException(Throwable cause) {
        super(msg, cause);
    }
}
