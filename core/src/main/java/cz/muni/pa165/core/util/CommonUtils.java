package cz.muni.pa165.core.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.exception.DataSeedException;
import java.io.IOException;
import java.util.List;
import org.springframework.core.io.ClassPathResource;

public class CommonUtils {

    public static <T> List<T> loadSeedJson(
        ObjectMapper objectMapper,
        String seedFile,
        TypeReference<List<T>> typeReference
    ) {
        List<T> objects;
        try {
            objects =
                objectMapper.readValue(
                    new ClassPathResource(seedFile).getInputStream(),
                    typeReference
                );
        } catch (IOException e) {
            throw new DataSeedException(e);
        }
        return objects;
    }
}
