# Rating microservice
Represents a microservice for managing ratings. Provides a REST API that allows for CRUD operations on ratings.

## Running the application
The microservice runs by default on `port 8080`. To run the microservice:
```shell
mvn clean install # compile the project first
mvn spring-boot:run
```
To run the microservice with custom port in case of port conflicts:
```shell
mvn spring-boot:run -Dspring-boot.run.arguments="--server.port=8003"
```

## Endpoints

### [GET] /swagger-ui/index.html
Swagger UI for the microservice. Open this in your browser to see the Open API documentation: <http://localhost:8003/swagger-ui/index.html>

### [GET] /api/v1/ratings/{id}
Get rating with specific ID.
```shell
curl localhost:8003/api/v1/ratings/1
```
Response:
```json
{
  "id": 1,
  "movieId": 1,
  "userId": 2,
  "acting": 3,
  "plot": 2,
  "visuals": 5,
  "sound": 4,
  "sfx": null,
  "overall": 1,
  "comment": "Terrible movie!"
}
```

### [GET] /api/v1/ratings
Get all saved ratings.
```shell
curl localhost:8003/api/v1/ratings
```
Response:
```json
{
  "content": [
    {
      "id": 1,
      "movieId": 1,
      "userId": 2,
      "acting": 3,
      "plot": 2,
      "visuals": 5,
      "sound": 4,
      "sfx": null,
      "overall": 1,
      "comment": "Terrible movie!"
    },
    {
      "id": 2,
      "movieId": 1,
      "userId": 1,
      "acting": 8,
      "plot": 7,
      "visuals": 10,
      "sound": 9,
      "sfx": null,
      "overall": 5,
      "comment": "Great movie!"
    },
    {
      "id": 3,
      "movieId": 2,
      "userId": 2,
      "acting": 5,
      "plot": 4,
      "visuals": 7,
      "sound": 6,
      "sfx": null,
      "overall": 3,
      "comment": "Not so great movie!"
    }
  ],
  "pageable": {
    "sort": {
      "empty": true,
      "sorted": false,
      "unsorted": true
    },
    "offset": 0,
    "pageNumber": 0,
    "pageSize": 20,
    "paged": true,
    "unpaged": false
  },
  "last": true,
  "totalPages": 1,
  "totalElements": 3,
  "size": 20,
  "number": 0,
  "sort": {
    "empty": true,
    "sorted": false,
    "unsorted": true
  },
  "first": true,
  "numberOfElements": 3,
  "empty": false
}
```

### [POST] /api/v1/ratings
Create a rating.
```shell
curl localhost:8003/api/v1/ratings -X POST -H "Content-Type: application/json" -d '{"id": null, "movieId": 2, "userId": 2, "overall": 3, "comment": "Not so great IMHO."}'
```
Response:
```json
{
  "id": 5,
  "movieId": 2,
  "userId": 2,
  "acting": null,
  "plot": null,
  "visuals": null,
  "sound": null,
  "sfx": null,
  "overall": 3,
  "comment": "Not so great IMHO."
}
```

### [PUT] /api/v1/ratings/{id}
Update a rating with specific ID.
```shell
curl localhost:8003/api/v1/ratings/5 -X PUT -H "Content-Type: application/json" -d '{"id": null, "movieId": 2, "userId": 2, "overall": 5, "comment": "Changed my mind, it is good."}'
```
Response:
```json
{
  "id": 5,
  "movieId": 2,
  "userId": 2,
  "acting": null,
  "plot": null,
  "visuals": null,
  "sound": null,
  "sfx": null,
  "overall": 5,
  "comment": "Changed my mind, it is good."
}
```

### [DELETE] /api/v1/ratings/{id}
Delete a rating with specific ID.
```shell
curl localhost:8003/api/v1/ratings/5 -X DELETE
```