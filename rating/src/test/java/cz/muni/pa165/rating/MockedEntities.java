package cz.muni.pa165.rating;

import cz.muni.pa165.rating.data.model.MovieRating;
import java.util.List;

public class MockedEntities {

    public static final MovieRating RATING_1 = new MovieRating(
        1L,
        1L,
        5L,
        7,
        8,
        9,
        10,
        8,
        9,
        "Great movie!"
    );
    public static final MovieRating RATING_2 = new MovieRating(
        2L,
        2L,
        5L,
        7,
        8,
        9,
        10,
        8,
        9,
        "Not great, not terrible."
    );
    public static final List<MovieRating> RATINGS = List.of(RATING_1, RATING_2);
}
