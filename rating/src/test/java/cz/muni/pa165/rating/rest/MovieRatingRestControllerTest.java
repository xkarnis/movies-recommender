package cz.muni.pa165.rating.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.rating.facade.MovieRatingFacade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("test")
@WebMvcTest(controllers = MovieRatingRestController.class)
@OverrideAutoConfiguration(enabled = true)
@AutoConfigureMockMvc(addFilters = false)
public class MovieRatingRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private MovieRatingFacade movieRatingFacade;

    @Value("${rest.path.ratings}")
    private String restPath;

    @Test
    void findAllTest() throws Exception {
        mockMvc
            .perform(
                get(restPath)
                    .param("page", "0")
                    .param("size", "5")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void findByIdTest() throws Exception {
        mockMvc
            .perform(
                get(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void createTest() throws Exception {
        var movieRatingDto = new MovieRatingDto();
        movieRatingDto.setMovieId(1L);
        movieRatingDto.setUserId(1L);
        movieRatingDto.setComment("This movie was awful!!!");
        mockMvc
            .perform(
                post(restPath)
                    .content(objectMapper.writeValueAsString(movieRatingDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isCreated());
    }

    @Test
    void updateTest() throws Exception {
        var movieRatingDto = new MovieRatingDto();
        movieRatingDto.setComment("Changed my mind, this movie was great!");
        mockMvc
            .perform(
                put(restPath + "/1")
                    .content(objectMapper.writeValueAsString(movieRatingDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void deleteTest() throws Exception {
        mockMvc
            .perform(
                delete(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNoContent());
    }
}
