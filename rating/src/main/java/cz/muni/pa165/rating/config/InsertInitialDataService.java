package cz.muni.pa165.rating.config;

import cz.muni.pa165.rating.data.model.MovieRating;
import cz.muni.pa165.rating.data.repository.MovieRatingRepository;
import jakarta.annotation.PostConstruct;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InsertInitialDataService {

    private final MovieRatingRepository movieRatingRepository;

    @Autowired
    public InsertInitialDataService(
        MovieRatingRepository movieRatingRepository
    ) {
        this.movieRatingRepository = movieRatingRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        insertDummyRatings();
    }

    private void insertDummyRatings() {
        final MovieRating rating1 = new MovieRating();
        rating1.setMovieId(1L);
        rating1.setUserId(1L);
        rating1.setOverall(5);
        rating1.setPlot(7);
        rating1.setActing(8);
        rating1.setSound(9);
        rating1.setVisuals(10);
        rating1.setComment("Great movie!");

        final MovieRating rating2 = new MovieRating();
        rating2.setMovieId(2L);
        rating2.setUserId(2L);
        rating2.setOverall(3);
        rating2.setPlot(4);
        rating2.setActing(5);
        rating2.setSound(6);
        rating2.setVisuals(7);
        rating2.setComment("Not so great movie!");

        final MovieRating rating3 = new MovieRating();
        rating3.setMovieId(1L);
        rating3.setUserId(2L);
        rating3.setOverall(1);
        rating3.setPlot(2);
        rating3.setActing(3);
        rating3.setSound(4);
        rating3.setVisuals(5);
        rating3.setComment("Terrible movie!");

        final Set<MovieRating> ratings = Set.of(rating1, rating2, rating3);
        movieRatingRepository.saveAll(ratings);
    }
}
