package cz.muni.pa165.rating.config;

import static cz.muni.pa165.core.constants.OAuthScopes.TEST_READ;
import static cz.muni.pa165.core.constants.OAuthScopes.TEST_WRITE;

import cz.muni.pa165.core.constants.AppSecurityScheme;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Slf4j
@SecurityScheme(
    name = AppSecurityScheme.NAME,
    type = SecuritySchemeType.HTTP,
    scheme = AppSecurityScheme.BEARER,
    description = AppSecurityScheme.DESCRIPTION
)
@Configuration
public class SecurityConfig {

    @Value("${rest.path.ratings}/**")
    private String restApiPath;

    @Bean
    @Profile("!insecure")
    SecurityFilterChain securityFilterChain(HttpSecurity http)
        throws Exception {
        log.info("Security is enabled");
        http
            .authorizeHttpRequests(matcherRegistry ->
                matcherRegistry
                    .requestMatchers(HttpMethod.GET, restApiPath)
                    .hasAuthority(prefixScope(TEST_READ))
                    .requestMatchers(HttpMethod.POST, restApiPath)
                    .hasAuthority(prefixScope(TEST_WRITE))
                    .requestMatchers(HttpMethod.PUT, restApiPath)
                    .hasAuthority(prefixScope(TEST_WRITE))
                    .requestMatchers(HttpMethod.DELETE, restApiPath)
                    .hasAuthority(prefixScope(TEST_WRITE))
                    .anyRequest()
                    .permitAll()
            )
            .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
            .csrf()
            .disable();
        return http.build();
    }

    @Bean
    @Profile("insecure")
    SecurityFilterChain securityFilterChainInsecure(HttpSecurity http)
        throws Exception {
        log.info("Security is disabled");
        http
            .authorizeHttpRequests(x -> x.anyRequest().permitAll())
            .csrf()
            .disable();
        return http.build();
    }

    private String prefixScope(String scope) {
        return "SCOPE_" + scope;
    }
}
