package cz.muni.pa165.rating.service;

import cz.muni.pa165.core.api.account.AccountDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.server.resource.web.reactive.function.client.ServletBearerExchangeFilterFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class WebClientAccountService {

    private final String accountPath;

    private final WebClient accountWebClient;

    public WebClientAccountService(
        @Value("${service.account.host}") String accountMicroserviceHost,
        @Value("${service.account.port}") String accountMicroservicePort,
        @Value("${service.account.rest.path.me}") String accountPath
    ) {
        this.accountPath = accountPath;
        accountWebClient =
            WebClient
                .builder()
                .filter(new ServletBearerExchangeFilterFunction())
                .baseUrl(
                    accountMicroserviceHost + ":" + accountMicroservicePort
                )
                .build();
    }

    public AccountDto getAccount() {
        return accountWebClient
            .get()
            .uri(accountPath)
            .retrieve()
            .bodyToMono(AccountDto.class)
            .block();
    }
}
