package cz.muni.pa165.rating.rest;

import cz.muni.pa165.core.api.account.AccountDto;
import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.core.constants.AppSecurityScheme;
import cz.muni.pa165.core.constants.OAuthScopes;
import cz.muni.pa165.rating.data.model.RatingFilter;
import cz.muni.pa165.rating.facade.MovieRatingFacade;
import cz.muni.pa165.rating.service.WebClientAccountService;
import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path = "${rest.path.ratings}")
public class MovieRatingRestController {

    private final MovieRatingFacade movieRatingFacade;
    private final ObservationRegistry observationRegistry;
    private final WebClientAccountService accountService;
    private final Environment environment;

    @Autowired
    public MovieRatingRestController(
        MovieRatingFacade movieRatingFacade,
        ObservationRegistry observationRegistry,
        WebClientAccountService accountService,
        Environment environment
    ) {
        this.movieRatingFacade = movieRatingFacade;
        this.observationRegistry = observationRegistry;
        this.accountService = accountService;
        this.environment = environment;
    }

    @Operation(
        summary = "Get all movie ratings",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<MovieRatingDto> getMovieRatings(
        @ParameterObject RatingFilter filter,
        @ParameterObject Pageable pageable
    ) {
        return Observation
            .createNotStarted("rating.getMovieRatings", observationRegistry)
            .lowCardinalityKeyValue(
                "page",
                String.valueOf(pageable.getPageNumber())
            )
            .lowCardinalityKeyValue(
                "size",
                String.valueOf(pageable.getPageSize())
            )
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieRatingFacade.findAll(filter, pageable));
    }

    @Operation(
        summary = "Get movie rating by id",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.OK)
    public MovieRatingDto getMovieRatingById(@PathVariable long id) {
        return Observation
            .createNotStarted("rating.getMovieRatingById", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieRatingFacade.findById(id));
    }

    @Operation(
        summary = "Create new movie rating",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MovieRatingDto createMovieRating(
        @Valid @RequestBody MovieRatingDto movieRating
    ) {
        if (!environment.acceptsProfiles(Profiles.of("insecure"))) {
            log.info("Associating rating with user account.");
            try {
                AccountDto account = accountService.getAccount();
                movieRating.setUserId(account.getId());
            } catch (Exception e) {
                throw new RuntimeException(
                    "User doesn't have an associated account."
                );
            }
        }

        return Observation
            .createNotStarted("rating.createMovieRating", observationRegistry)
            .lowCardinalityKeyValue(
                "movieId",
                String.valueOf(movieRating.getMovieId())
            )
            .lowCardinalityKeyValue(
                "userId",
                String.valueOf(movieRating.getUserId())
            )
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieRatingFacade.create(movieRating));
    }

    @Operation(
        summary = "Update movie rating",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PutMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.OK)
    public MovieRatingDto updateMovieRating(
        @PathVariable long id,
        @RequestBody MovieRatingDto movieRating
    ) {
        return Observation
            .createNotStarted("rating.updateMovieRating", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .lowCardinalityKeyValue(
                "movieId",
                String.valueOf(movieRating.getMovieId())
            )
            .lowCardinalityKeyValue(
                "userId",
                String.valueOf(movieRating.getUserId())
            )
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieRatingFacade.update(id, movieRating));
    }

    @Operation(
        summary = "Delete movie rating",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @DeleteMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMovieRating(@PathVariable long id) {
        Observation
            .createNotStarted("rating.deleteMovieRating", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieRatingFacade.delete(id));
    }
}
