package cz.muni.pa165.rating.service;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.rating.data.model.MovieRating;
import cz.muni.pa165.rating.data.model.RatingFilter;
import cz.muni.pa165.rating.data.repository.MovieRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MovieRatingService {

    private final MovieRatingRepository movieRatingRepository;

    @Autowired
    public MovieRatingService(MovieRatingRepository movieRatingRepository) {
        this.movieRatingRepository = movieRatingRepository;
    }

    @Transactional(readOnly = true)
    public MovieRating findById(Long id) {
        return movieRatingRepository
            .findById(id)
            .orElseThrow(() ->
                new ResourceNotFoundException(
                    "MovieRating with id: " + id + " was not found."
                )
            );
    }

    @Transactional(readOnly = true)
    public Page<MovieRating> findAll(RatingFilter filter, Pageable pageable) {
        return movieRatingRepository.findAllFiltered(filter, pageable);
    }

    public MovieRating create(MovieRating movieRating) {
        return movieRatingRepository.save(movieRating);
    }

    public MovieRating update(MovieRating movieRating) {
        // check if movieRating exists
        findById(movieRating.getId());
        return movieRatingRepository.save(movieRating);
    }

    public void delete(MovieRating movieRating) {
        movieRatingRepository.delete(movieRating);
    }
}
