package cz.muni.pa165.rating.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class MovieRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long movieId;

    @Column
    private Long userId;

    @Column
    private Integer acting;

    @Column
    private Integer plot;

    @Column
    private Integer visuals;

    @Column
    private Integer sound;

    @Column
    private Integer sfx;

    @Column
    private Integer overall;

    @Column
    private String comment;
}
