package cz.muni.pa165.rating.facade;

import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.rating.data.model.RatingFilter;
import cz.muni.pa165.rating.mapper.MovieRatingMapper;
import cz.muni.pa165.rating.service.MovieRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MovieRatingFacade {

    private final MovieRatingService movieRatingService;
    private final MovieRatingMapper movieRatingMapper;

    @Autowired
    public MovieRatingFacade(
        MovieRatingService movieRatingService,
        MovieRatingMapper movieRatingMapper
    ) {
        this.movieRatingService = movieRatingService;
        this.movieRatingMapper = movieRatingMapper;
    }

    @Transactional(readOnly = true)
    public MovieRatingDto findById(Long id) {
        return movieRatingMapper.mapToDto(movieRatingService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<MovieRatingDto> findAll(
        RatingFilter filter,
        Pageable pageable
    ) {
        return movieRatingMapper.mapToPageDto(
            movieRatingService.findAll(filter, pageable)
        );
    }

    @Transactional
    public MovieRatingDto create(MovieRatingDto movieRating) {
        return movieRatingMapper.mapToDto(
            movieRatingService.create(
                movieRatingMapper.mapToEntity(movieRating)
            )
        );
    }

    @Transactional
    public MovieRatingDto update(Long id, MovieRatingDto movieRating) {
        movieRating.setId(id);
        return movieRatingMapper.mapToDto(
            movieRatingService.update(
                movieRatingMapper.mapToEntity(movieRating)
            )
        );
    }

    @Transactional
    public void delete(Long id) {
        movieRatingService.delete(movieRatingService.findById(id));
    }
}
