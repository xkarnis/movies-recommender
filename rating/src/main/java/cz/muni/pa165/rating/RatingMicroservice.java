package cz.muni.pa165.rating;

import cz.muni.pa165.rating.config.SecurityConfig;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@OpenAPIDefinition(
    info = @Info(
        title = "Movie Microservice",
        version = "1.0",
        description = """
                        REST API for the rating microservice. It provides access to the movie ratings.
                        """
    )
)
@ComponentScan(basePackages = "cz.muni.pa165")
@EnableJpaRepositories
@SpringBootApplication
public class RatingMicroservice {

    SecurityConfig securityConfig;

    @Autowired
    RatingMicroservice(SecurityConfig securityConfig) {
        this.securityConfig = securityConfig;
    }

    public static void main(String[] args) {
        SpringApplication.run(RatingMicroservice.class, args);
    }
}
