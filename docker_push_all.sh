#!/bin/bash
# Push docker images for all services

TAG=${1:-$(git branch --show-current)} # you can specify tag as first argument, otherwise current branch name is used

for SERVICE in {movie,account,genre,rating} ; do
    docker push quay.io/pa165/movie-recommender-"$SERVICE:$TAG"
done
