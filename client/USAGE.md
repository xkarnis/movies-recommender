# OAuth Client

Represents a client for getting OAuth tokens from the OAuth server. Provides a very simple UI for log-in and getting the actual value of the token.

## Running the application

```shell
mvn clean install # compile the project first
mvn spring-boot:run
```
To run the microservice with custom port in case of port conflicts:
```shell
mvn spring-boot:run -Dspring-boot.run.arguments="--server.port=8000"
```

## Usage

1. Open the client in the browser: <http://localhost:8000>
2. Log in and select wanted scopes
3. Click on **See access token** to display access token
