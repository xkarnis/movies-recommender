package cz.muni.pa165.client;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(Model model, @AuthenticationPrincipal OidcUser user) {
        model.addAttribute("user", user);
        return "index";
    }

    @GetMapping("/token")
    public String token(
        Model model,
        @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client
    ) {
        OAuth2AccessToken accessToken = oauth2Client.getAccessToken();
        model.addAttribute("accessToken", accessToken.getTokenValue());

        return "token";
    }
}
