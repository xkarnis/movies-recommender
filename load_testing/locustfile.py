import time
from json import JSONDecodeError

from locust import HttpUser, task, between


MOVIE_API = ":8000/api/v1/movies"
RATING_API = ":8003/api/v1/ratings"


class MovieRecommenderUser(HttpUser):
    wait_time = between(1, 5)

    # Movie requests
    @task
    def looks_at_all_movies(self):
        self.client.get(MOVIE_API)

    @task
    def finds_specific_movie(self):
        with self.client.get(MOVIE_API, catch_response=True) as response:
            try:
                movie_id = str(response.json()["content"][0]["id"])
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")
                return
            except KeyError:
                response.failure("Response did not contain expected key value")
                return
        self.client.get(MOVIE_API + "/" + movie_id)

    def looks_at_movie_recommendations(self):
        with self.client.get(MOVIE_API, catch_response=True) as response:
            try:
                movie_id = str(response.json()["content"][0]["id"])
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")
                return
            except KeyError:
                response.failure("Response did not contain expected key value")
                return
        self.client.get(MOVIE_API + "/" + movie_id + "/recommendations")

    # Rating requests
    @task
    def looks_at_ratings_of_movie(self):
        self.client.get(RATING_API + "?movieId=1")

    @task
    def looks_at_rating_of_user(self):
        self.client.get(RATING_API + "?userIds=1")

    @task
    def edit_rating(self):  # we pretend here that we are editing our own rating
        with self.client.get(RATING_API, catch_response=True) as response:
            try:
                parsed = response.json()["content"][0]
                rating_id = str(parsed["id"])
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")
                return
            except KeyError:
                response.failure("Response did not contain expected key value")
                return
            self.client.put(RATING_API + "/" + rating_id, json=parsed)



