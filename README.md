# Movies Recommender Catalogue

Project for PA165 - Java Enterprise Applications

Movie recommender is a web application that allows users to rate movies and get recommendations based on their ratings.

## About Project

- **Name**: Movies Recommender Catalogue
- **Technologies**: Java 11, Maven, Spring Boot, JPA, Lombok
- **Developers**:
    - Filip Karniš @xkarnis - _Project Leader_
    - Pavol Baran @xbaran4
    - Michal Drobňák  @xdrobnak
- **[Assignment](./ASSIGNMENT.md)**


### Building the application
The application is built using maven. If you don't have maven available replace `mvn` with `./mvnw` or `./mvnw.cmd` on Windows. This applies for other commands as well. To build the application, run the following command in the root directory of the project:
```shell
mvn clean install
```
After the previous command you can build docker images, run the following command in the root directory of the project. It uses current git branch name as a tag for the images, if you want to use a custom tag, you can pass it as an argument to the script:
```shell
./docker_build_all.sh
```
Or you can build docker images using docker-compose (uses `latest` tag):
```shell
docker-compose build
```
To push all images to a remote image registry, run the following command in the root directory of the project. Similar to the previous command, you can pass a custom tag as an argument to the script:
```shell
./docker_push_all.sh
```

### Testing the application
To test the application, run the following command in the root directory of the project, this runs test in all submodules:
```shell
mvn test
```
### Running the application
The application consists of multiple microservices runnable as separate applications. To run a microservice, run the following from root directory of the project:

Using maven:
```shell
cd microservice # where microservice is on of [account, genre, movie, rating]
mvn spring-boot:run

mvn spring-boot:run -Dspring-boot.run.profiles=insecure # run the microservice without OAuth
```
Using docker (after building docker images):
```shell
docker-compose up -d # start the microservices

docker-compose --file docker-compose-insecure.yaml up -d # start the microservices without OAuth
```

### Monitoring
Monitoring is done using prometheus and grafana.
They are running by default using `docker-compose`.

Prometheus is then available at http://localhost:9090.

Grafana is then available at http://localhost:3000.

#### Set up monitoring without docker-compose
Start prometheus (has to be executed from the root directory of the project 
(or you will have to change the path to the `prometheus.yml` file)):
```shell
docker run --rm --name prometheus -p 9090:9090 --network host \
  -v ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml:Z prom/prometheus:v2.43.0 \
  --config.file=/etc/prometheus/prometheus.yml
```

Start grafana:
```shell
docker run --rm -p 3000:3000 --network host -v ./grafana/provisioning/:/etc/grafana/provisioning/ \
 grafana/grafana:9.1.7
```
Add prometheus as a data source in grafana settings (http://localhost:9090).

You can import `grafane/provisioning/dashboards/grafana.json` dashboard to grafana to see some metrics.

### Seeding the databases
All microservices' databases can be seeded and cleared through the `/data` endpoint. As this endpoint should be used just for testing purposes, it does not require any authorization.
For seeding, run:
```shell
curl $HOST/data -X PUT
```
For clearing the data, run:
```shell
curl $HOST/data -X DELETE
```

### Running a defined test scenario
We define a load testing scenario with the `Locust` testing tool. The scenario focuses on 
the interaction `only` of a `regular user` with the Movie Recommender Platform. This mean that the scenario
is mostly composed of calls to the Movies API to find a certain movie or get recommendations, or Ratings API to look
at rating of a movie or edit the users ratings.

Before running test, install locust:
```shell 
pip3 install locust
```

To run the testing scenario:
```shell
cd load_testing
./run_test.sh
```
Open locust web UI at `http://0.0.0.0:8089` and insert 50 user, 10 per second and url ` http://localhost`
You should see something like this:
![Load Testing](./uml/load_screenshot.png)

## Components
The application consists of the following maven submodules:

### account
Represents a microservice for managing user accounts. Provides a REST API that allows for CRUD operations on accounts. For usage instructions, see [USAGE.md](./account/USAGE.md).


### core
Represents shared code between other modules. Contains DTOs, exceptions, and other classes that are used by other modules. This module is not runnable as a standalone application.

### genre
Represents a microservice for managing movie genres. Provides a REST API that allows for CRUD operations on genres. For usage instructions, see [USAGE.md](./genre/USAGE.md).

### movie
Represents a microservice for managing movies. Provides a REST API that allows for CRUD operations on movies. For usage instructions, see [USAGE.md](./movie/USAGE.md).

### rating
Represents a microservice for managing movie ratings. Provides a REST API that allows for CRUD operations on ratings. For usage instructions, see [USAGE.md](./rating/USAGE.md).

### client

Represents a client for getting OAuth tokens from the OAuth server. Provides a very simple UI for log-in and getting the actual value of the token. For usage instructions, see [USAGE.md](./client/USAGE.md).

## Roles
System has two user roles - **Admin** and **Generic User**.

* **Admin** is a role for a system administrator. This user has the ability to create, read, update and delete movies or genres into the catalogue.
* **Generic User** is self-explanatory. This user can view read movie catalogue, create ratings for movies and ask the system to recommend similar movies to a certain (selected) movie.

## Entities
* **Account** - represents an account of an admin or generic user.
* **Movie** - represents a movie.
* **Rating** - represents ratings that correspond to certain movie.
* **Genre** - represents a genre of a movie.


## Class diagram for DTOs

![Class diagram](./uml/class-diagram.png)

## Use case diagram

![Use case diagram](./uml/use-case-diagram.png)
