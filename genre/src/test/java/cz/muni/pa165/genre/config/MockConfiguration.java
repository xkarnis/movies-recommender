package cz.muni.pa165.genre.config;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.genre.MockedEntities;
import cz.muni.pa165.genre.data.repository.GenreRepository;
import cz.muni.pa165.genre.service.GenreService;
import cz.muni.pa165.genre.service.GenreServiceImpl;
import jakarta.annotation.PostConstruct;
import java.util.Optional;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@TestConfiguration
public class MockConfiguration {

    @Mock
    private GenreRepository repository;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockRepositoryMethods();
    }

    @Bean
    @Primary
    public GenreService genreService() {
        return new GenreServiceImpl(repository);
    }

    private void mockRepositoryMethods() {
        Mockito
            .doThrow(ResourceNotFoundException.class)
            .when(repository)
            .findById(Mockito.anyLong());
        Mockito
            .doReturn(Optional.of(MockedEntities.ACTION))
            .when(repository)
            .findById(MockedEntities.ACTION.getId());
        Mockito
            .doReturn(Optional.of(MockedEntities.ACTION))
            .when(repository)
            .findById(1L);

        Mockito
            .doReturn(new PageImpl<>(MockedEntities.GENRE_LIST))
            .when(repository)
            .findAll(Mockito.any(Pageable.class));

        Mockito
            .doReturn(MockedEntities.ACTION)
            .when(repository)
            .save(MockedEntities.ACTION);
    }
}
