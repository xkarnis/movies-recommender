package cz.muni.pa165.genre.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.util.CommonUtils;
import cz.muni.pa165.genre.data.model.Genre;
import cz.muni.pa165.genre.data.repository.GenreRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DataSeederServiceImpl implements DataSeederService {

    private final GenreRepository genreRepository;

    @Value("${rest.data.seed.genres}")
    private String genresSeedFile;

    @Autowired
    public DataSeederServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    @Transactional
    public void seedData() {
        clearData();
        List<Genre> genres = CommonUtils.loadSeedJson(
            new ObjectMapper(),
            genresSeedFile,
            new TypeReference<>() {}
        );
        genreRepository.saveAll(genres);
    }

    @Override
    @Transactional
    public void clearData() {
        genreRepository.deleteAll();
        genreRepository.flush();
    }
}
