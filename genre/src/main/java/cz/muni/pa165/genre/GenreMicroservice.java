package cz.muni.pa165.genre;

import cz.muni.pa165.genre.config.SecurityConfig;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@OpenAPIDefinition(
    info = @Info(
        title = "Movie genre microservice",
        version = "1.0",
        description = """
                        Simple service for genre manipulation. The API has operations for:
                        - getting all genres (with pagination)
                        - creating a new genre
                        - getting a specific genre by its id
                        """,
        contact = @Contact(name = "Michal Drobňák", email = "493239@muni.cz")
    )
)
@Tag(name = "Genre", description = "Microservice for movie genres") // metadata for inclusion into OpenAPI document
@ComponentScan(basePackages = "cz.muni.pa165")
@EnableJpaRepositories
@SpringBootApplication
public class GenreMicroservice {

    SecurityConfig securityConfig;

    @Autowired
    GenreMicroservice(SecurityConfig securityConfig) {
        this.securityConfig = securityConfig;
    }

    public static void main(String[] args) {
        SpringApplication.run(GenreMicroservice.class, args);
    }
}
