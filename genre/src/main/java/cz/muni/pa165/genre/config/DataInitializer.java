package cz.muni.pa165.genre.config;

import cz.muni.pa165.genre.data.model.Genre;
import cz.muni.pa165.genre.data.repository.GenreRepository;
import jakarta.annotation.PostConstruct;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DataInitializer {

    private final GenreRepository genreRepository;

    @Autowired
    public DataInitializer(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        final Genre action = new Genre();
        action.setName("Action");

        final Genre comedy = new Genre();
        comedy.setName("Comedy");

        final Genre drama = new Genre();
        drama.setName("Drama");

        final Genre horror = new Genre();
        horror.setName("Horror");

        final Genre romance = new Genre();
        romance.setName("Romance");

        final Set<Genre> genres = Set.of(
            action,
            comedy,
            drama,
            horror,
            romance
        );

        genreRepository.saveAll(genres);
    }
}
