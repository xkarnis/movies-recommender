package cz.muni.pa165.genre.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.core.util.CommonUtils;
import cz.muni.pa165.genre.data.model.Genre;
import cz.muni.pa165.genre.data.repository.GenreRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;

    @Value("${rest.data.seed.genres}")
    private String genresSeedFile;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Genre> findAll(Pageable pageable) {
        return genreRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Genre findById(Long id) {
        return genreRepository
            .findById(id)
            .orElseThrow(() ->
                new ResourceNotFoundException(
                    "Genre with id: " + id + " was not found."
                )
            );
    }

    @Override
    @Transactional
    public Genre create(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    @Transactional
    public Genre update(Genre genre) {
        findById(genre.getId());
        return genreRepository.save(genre);
    }

    @Override
    @Transactional
    public void delete(Genre genre) {
        genreRepository.delete(genre);
    }
}
