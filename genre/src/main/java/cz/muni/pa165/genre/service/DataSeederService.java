package cz.muni.pa165.genre.service;

import org.springframework.transaction.annotation.Transactional;

public interface DataSeederService {
    @Transactional
    void seedData();

    @Transactional
    void clearData();
}
