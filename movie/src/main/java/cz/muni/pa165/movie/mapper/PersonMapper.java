package cz.muni.pa165.movie.mapper;

import cz.muni.pa165.core.api.movie.PersonDto;
import cz.muni.pa165.movie.data.model.Person;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Mapper(componentModel = "spring")
public interface PersonMapper {
    PersonDto mapToDto(Person person);

    List<PersonDto> mapToList(List<Person> persons);

    default Page<PersonDto> mapToPageDto(Page<Person> persons) {
        return new PageImpl<>(
            mapToList(persons.getContent()),
            persons.getPageable(),
            persons.getTotalPages()
        );
    }

    Person mapToEntity(PersonDto person);
}
