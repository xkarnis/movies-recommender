package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.repository.MovieFilter;
import cz.muni.pa165.movie.data.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MovieService {

    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Transactional(readOnly = true)
    public Movie findById(Long id) {
        return movieRepository
            .findById(id)
            .orElseThrow(() ->
                new ResourceNotFoundException(
                    "Movie with id: " + id + " was not found."
                )
            );
    }

    @Transactional(readOnly = true)
    public Page<Movie> findAll(Pageable pageable, MovieFilter movieFilter) {
        if (movieFilter == null || movieFilter.isEmpty()) { // if no filter is set, avoid heavy query processing
            return movieRepository.findAll(pageable);
        }
        return movieRepository.findByTitleContainingAndDirector_SurnameContainingAndActors_SurnameContaining(
            pageable,
            movieFilter.getTitle(),
            movieFilter.getDirectorSurname(),
            movieFilter.getActorSurname()
        );
    }

    public Movie create(Movie movie) {
        return movieRepository.save(movie);
    }

    public Movie update(Movie movie) {
        // check if movie exists
        findById(movie.getId());
        return movieRepository.save(movie);
    }

    public void delete(Movie movie) {
        movieRepository.delete(movie);
    }
}
