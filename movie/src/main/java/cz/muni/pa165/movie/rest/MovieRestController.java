package cz.muni.pa165.movie.rest;

import cz.muni.pa165.core.api.movie.MovieBasicViewDto;
import cz.muni.pa165.core.api.movie.MovieDetailedViewDto;
import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.core.constants.AppSecurityScheme;
import cz.muni.pa165.core.constants.OAuthScopes;
import cz.muni.pa165.movie.data.repository.MovieFilter;
import cz.muni.pa165.movie.facade.MovieFacade;
import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import java.util.List;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${rest.path.movies}")
public class MovieRestController {

    private final MovieFacade movieFacade;
    private final ObservationRegistry observationRegistry;

    @Autowired
    public MovieRestController(
        MovieFacade movieFacade,
        ObservationRegistry observationRegistry
    ) {
        this.movieFacade = movieFacade;
        this.observationRegistry = observationRegistry;
    }

    @Operation(
        summary = "Get all movies",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<MovieBasicViewDto> getMovies(
        @ParameterObject Pageable pageable,
        @ParameterObject MovieFilter movieFilter
    ) {
        final Page<MovieBasicViewDto> page = Observation
            .createNotStarted("movie.getMovies", observationRegistry)
            .lowCardinalityKeyValue(
                "page",
                String.valueOf(pageable.getPageNumber())
            )
            .lowCardinalityKeyValue(
                "size",
                String.valueOf(pageable.getPageSize())
            )
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.findAll(pageable, movieFilter));
        return page;
    }

    @Operation(
        summary = "Get movie by id",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.OK)
    public MovieDetailedViewDto getMovieById(@PathVariable long id) {
        final MovieDetailedViewDto dto = Observation
            .createNotStarted("movie.getMovieById", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.findById(id));
        return dto;
    }

    @Operation(
        summary = "Create new movie",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MovieDetailedViewDto createMovie(
        @Valid @RequestBody MovieDetailedViewDto movie
    ) {
        final MovieDetailedViewDto dto = Observation
            .createNotStarted("movie.createMovie", observationRegistry)
            .lowCardinalityKeyValue("title", movie.getTitle())
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.create(movie));
        return dto;
    }

    @Operation(
        summary = "Update movie",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PutMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.OK)
    public MovieDetailedViewDto updateMovie(
        @PathVariable long id,
        @RequestBody MovieDetailedViewDto movie
    ) {
        final MovieDetailedViewDto dto = Observation
            .createNotStarted("movie.updateMovie", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .lowCardinalityKeyValue("title", movie.getTitle())
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.update(id, movie));
        return dto;
    }

    @Operation(
        summary = "Delete movie",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @DeleteMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMovie(@PathVariable long id) {
        Observation
            .createNotStarted("movie.deleteMovie", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.delete(id));
    }

    @Operation(
        summary = "Create new movie rating",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PostMapping(path = "${rest.path.id}/ratings")
    @ResponseStatus(HttpStatus.CREATED)
    public MovieRatingDto createMovieRating(
        @PathVariable long id,
        @RequestBody MovieRatingDto movieRating
    ) {
        movieRating.setMovieId(id);

        final MovieRatingDto dto = Observation
            .createNotStarted("movie.createMovieRating", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .lowCardinalityKeyValue(
                "user_id",
                String.valueOf(movieRating.getUserId())
            )
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.createRating(movieRating));
        return dto;
    }

    @Operation(
        summary = "Get recommendations",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping(path = "${rest.path.id}/recommendations")
    @ResponseStatus(HttpStatus.OK)
    public List<MovieBasicViewDto> getMovieRecommendations(
        @PathVariable long id
    ) {
        final List<MovieBasicViewDto> list = Observation
            .createNotStarted(
                "movie.getMovieRecommendations",
                observationRegistry
            )
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> movieFacade.recommend(id));
        return list;
    }
}
