package cz.muni.pa165.movie.data.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(exclude = { "moviesDirected", "moviesActedIn" })
@ToString(exclude = { "moviesDirected", "moviesActedIn" })
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String surname;

    @OneToMany(
        mappedBy = "director",
        cascade = { CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE }
    )
    private Set<Movie> moviesDirected = new HashSet<>();

    @ManyToMany(
        fetch = FetchType.LAZY,
        mappedBy = "actors",
        cascade = { CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE }
    )
    private Set<Movie> moviesActedIn = new HashSet<>();
}
