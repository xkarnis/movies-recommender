package cz.muni.pa165.movie.config;

import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.model.Person;
import cz.muni.pa165.movie.data.repository.MovieRepository;
import cz.muni.pa165.movie.data.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InsertInitialDataService {

    private final MovieRepository movieRepository;
    private final PersonRepository personRepository;

    @Autowired
    public InsertInitialDataService(
        MovieRepository movieRepository,
        PersonRepository personRepository
    ) {
        this.movieRepository = movieRepository;
        this.personRepository = personRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        insertPersonWithContact();
        insertDummyPersons();
    }

    private void insertPersonWithContact() {
        final Person actor1 = new Person();
        actor1.setName("Johnny");
        actor1.setSurname("Depp");

        final Person actor2 = new Person();
        actor2.setName("Orlando");
        actor2.setSurname("Bloom");

        final Person actor3 = new Person();
        actor3.setName("Keira");
        actor3.setSurname("Knightley");

        final Set<Person> actors = Set.of(actor1, actor2, actor3);

        final Person actor4 = new Person();
        actor4.setName("Jack");
        actor4.setSurname("Black");

        final Person actor5 = new Person();
        actor5.setName("Dwayne");
        actor5.setSurname("Johnson");

        final Set<Person> actors2 = Set.of(actor4, actor5);

        final Person director = new Person();
        director.setName("Gore");
        director.setSurname("Verbinski");

        final Person director2 = new Person();
        director2.setName("Harry");
        director2.setSurname("Longnose");

        final Movie movie1 = new Movie();
        movie1.setTitle("Pirates of the Caribbean");
        movie1.setDescription(
            "A pirate captain and his crew are in search of a legendary treasure."
        );

        final Movie movie2 = new Movie();
        movie2.setTitle("Random movie");
        movie2.setDescription("A movie where random things are happening.");

        movie1.setActors(actors);
        movie1.setDirector(director);
        movie1.setGenreIds(Set.of(1L, 5L));
        movie1.setImages(
            Set.of(
                "https://example.com/image1.jpg",
                "https://example.com/image2.jpg"
            )
        );

        director.getMoviesDirected().add(movie1);
        actors.forEach(actor -> actor.getMoviesActedIn().add(movie1));

        movie2.setActors(actors2);
        movie2.setDirector(director2);
        director2.getMoviesDirected().add(movie2);
        actors2.forEach(actor -> actor.getMoviesActedIn().add(movie2));

        personRepository.saveAll(actors);
        personRepository.saveAll(actors2);
        personRepository.save(director);
        personRepository.save(director2);
        movieRepository.save(movie1);
        movieRepository.save(movie2);
    }

    private void insertDummyPersons() {
        final List<Person> persons = new ArrayList<>(40);
        for (int i = 0; i < 40; i++) {
            final Person person = new Person();
            person.setName("John" + i);
            person.setSurname("Doe" + i);
            persons.add(person);
        }
        personRepository.saveAll(persons);
    }
}
