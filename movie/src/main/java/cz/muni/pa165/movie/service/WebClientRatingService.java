package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.core.api.rating.RatingsPageableResponse;
import java.net.URI;
import java.util.List;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.server.resource.web.reactive.function.client.ServletBearerExchangeFilterFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

@Service
public class WebClientRatingService {

    private static final int RATINGS_PAGE_SIZE = 10;
    private final WebClient ratingsWebClient;
    private final String ratingsPath;

    WebClientRatingService(
        @Value("${service.rating.host}") String ratingMicroserviceHost,
        @Value("${service.rating.port}") String ratingMicroservicePort,
        @Value("${service.rating.rest.path.ratings}") String ratingsPath
    ) {
        this.ratingsPath = ratingsPath;
        ratingsWebClient =
            WebClient
                .builder()
                .filter(new ServletBearerExchangeFilterFunction())
                .baseUrl(ratingMicroserviceHost + ":" + ratingMicroservicePort)
                .build();
    }

    public RatingsPageableResponse getMovieRatings(
        Long id,
        int positivityThreshold,
        int page
    ) {
        return getRatings(uriBuilder ->
            uriBuilder
                .path(ratingsPath)
                .queryParam("movieId", id)
                .queryParam("overall", positivityThreshold)
                .queryParam("page", page)
                .queryParam("size", RATINGS_PAGE_SIZE)
                .build()
        );
    }

    public RatingsPageableResponse getUserRatings(
        List<Long> ids,
        int positivityThreshold,
        int page
    ) {
        return getRatings(uriBuilder ->
            uriBuilder
                .path(ratingsPath)
                .queryParam("userIds", ids)
                .queryParam("overall", positivityThreshold)
                .queryParam("page", page)
                .queryParam("size", RATINGS_PAGE_SIZE)
                .build()
        );
    }

    public MovieRatingDto createRating(MovieRatingDto movieRatingDto) {
        return ratingsWebClient
            .post()
            .uri(ratingsPath)
            .body(Mono.just(movieRatingDto), MovieRatingDto.class)
            .retrieve()
            .bodyToMono(MovieRatingDto.class)
            .block();
    }

    private RatingsPageableResponse getRatings(
        Function<UriBuilder, URI> buildUriFunc
    ) {
        return ratingsWebClient
            .get()
            .uri(buildUriFunc)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(RatingsPageableResponse.class)
            .block();
    }
}
