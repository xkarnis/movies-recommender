package cz.muni.pa165.movie.config;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.movie.MockedEntities;
import cz.muni.pa165.movie.data.repository.MovieRepository;
import cz.muni.pa165.movie.service.MovieService;
import jakarta.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@TestConfiguration
public class MovieTestConfiguration {

    private final MovieRepository repository = Mockito.mock(
        MovieRepository.class
    );

    @Bean
    public MovieService movieService() {
        return new MovieService(repository);
    }

    @PostConstruct
    public void setup() {
        mockRepositoryMethods();
    }

    private void mockRepositoryMethods() {
        Mockito
            .doThrow(ResourceNotFoundException.class)
            .when(repository)
            .findById(Mockito.anyLong());
        Mockito
            .doReturn(Optional.of(MockedEntities.PIRATES_OF_THE_CARIBBEAN))
            .when(repository)
            .findById(MockedEntities.PIRATES_OF_THE_CARIBBEAN.getId());

        Mockito
            .doReturn(new PageImpl<>(MockedEntities.MOVIES))
            .when(repository)
            .findAll(Mockito.any(Pageable.class));
        Mockito
            .doReturn(new PageImpl<>(List.of()))
            .when(repository)
            .findByTitleContainingAndDirector_SurnameContainingAndActors_SurnameContaining(
                Mockito.any(Pageable.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
            );
        Mockito
            .doReturn(
                new PageImpl<>(List.of(MockedEntities.PIRATES_OF_THE_CARIBBEAN))
            )
            .when(repository)
            .findByTitleContainingAndDirector_SurnameContainingAndActors_SurnameContaining(
                Mockito.any(Pageable.class),
                Mockito.contains(
                    MockedEntities.PIRATES_OF_THE_CARIBBEAN
                        .getTitle()
                        .substring(2, 8)
                ),
                Mockito.anyString(),
                Mockito.anyString()
            );
        Mockito
            .doReturn(
                new PageImpl<>(
                    List.of(
                        MockedEntities.PIRATES_OF_THE_CARIBBEAN,
                        MockedEntities.RANGO
                    )
                )
            )
            .when(repository)
            .findByTitleContainingAndDirector_SurnameContainingAndActors_SurnameContaining(
                Mockito.any(Pageable.class),
                Mockito.anyString(),
                Mockito.contains(
                    MockedEntities.GORE_VERBINSKI.getSurname().substring(2, 8)
                ),
                Mockito.anyString()
            );
        Mockito
            .doReturn(
                new PageImpl<>(
                    List.of(
                        MockedEntities.PIRATES_OF_THE_CARIBBEAN,
                        MockedEntities.RANGO
                    )
                )
            )
            .when(repository)
            .findByTitleContainingAndDirector_SurnameContainingAndActors_SurnameContaining(
                Mockito.any(Pageable.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.contains(
                    MockedEntities.JOHNNY_DEPP.getSurname().substring(2, 4)
                )
            );
        Mockito
            .doReturn(MockedEntities.PIRATES_OF_THE_CARIBBEAN)
            .when(repository)
            .save(MockedEntities.PIRATES_OF_THE_CARIBBEAN);
        Mockito
            .doReturn(MockedEntities.PIRATES_OF_THE_CARIBBEAN)
            .when(repository)
            .save(MockedEntities.PIRATES_OF_THE_CARIBBEAN);
    }
}
