package cz.muni.pa165.movie.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.core.api.rating.RatingsPageableResponse;
import java.io.IOException;
import java.util.List;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
public class WebClientRatingServiceTest {

    private static MockWebServer mockWebServer;
    private static WebClientRatingService ratingService;

    private final ObjectMapper mapper;

    @Autowired
    WebClientRatingServiceTest(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        ratingService =
            new WebClientRatingService(
                "http://localhost",
                Integer.toString(mockWebServer.getPort()),
                "rating"
            );
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void getMovieRatings() throws Exception {
        //given
        var ratingDto = new MovieRatingDto();
        ratingDto.setId(666L);
        ratingDto.setMovieId(1L);
        ratingDto.setUserId(2L);

        var mockedPage = new RatingsPageableResponse(
            List.of(ratingDto),
            1,
            1,
            1L
        );

        mockWebServer.enqueue(
            new MockResponse()
                .setBody(mapper.writeValueAsString(mockedPage))
                .addHeader("Content-Type", "application/json")
        );

        //when
        var responsePage = ratingService.getMovieRatings(1L, 1, 1);

        //then
        Assertions.assertEquals(
            "/rating?movieId=" +
            ratingDto.getMovieId() +
            "&overall=1&page=1&size=10",
            mockWebServer.takeRequest().getPath()
        );
        Assertions.assertEquals(
            ratingDto.getId(),
            responsePage.getContent().get(0).getId()
        );
        Assertions.assertEquals(
            ratingDto.getMovieId(),
            responsePage.getContent().get(0).getMovieId()
        );
        Assertions.assertEquals(
            ratingDto.getUserId(),
            responsePage.getContent().get(0).getUserId()
        );
    }

    @Test
    void getUserRatings() throws Exception {
        //given
        var ratingDto = new MovieRatingDto();
        ratingDto.setId(666L);
        ratingDto.setMovieId(1L);
        ratingDto.setUserId(2L);

        var mockedPage = new RatingsPageableResponse(
            List.of(ratingDto),
            1,
            1,
            1L
        );

        mockWebServer.enqueue(
            new MockResponse()
                .setBody(mapper.writeValueAsString(mockedPage))
                .addHeader("Content-Type", "application/json")
        );

        //when
        var responsePage = ratingService.getUserRatings(
            List.of(ratingDto.getUserId()),
            1,
            1
        );

        //then
        Assertions.assertEquals(
            "/rating?userIds=" +
            ratingDto.getUserId() +
            "&overall=1&page=1&size=10",
            mockWebServer.takeRequest().getPath()
        );
        Assertions.assertEquals(
            ratingDto.getId(),
            responsePage.getContent().get(0).getId()
        );
        Assertions.assertEquals(
            ratingDto.getMovieId(),
            responsePage.getContent().get(0).getMovieId()
        );
        Assertions.assertEquals(
            ratingDto.getUserId(),
            responsePage.getContent().get(0).getUserId()
        );
    }

    @Test
    public void createRating()
        throws JsonProcessingException, InterruptedException {
        //given
        var ratingDto = new MovieRatingDto();
        ratingDto.setMovieId(1L);
        ratingDto.setUserId(2L);
        ratingDto.setOverall(9);
        ratingDto.setPlot(7);
        ratingDto.setActing(8);
        ratingDto.setVisuals(9);
        ratingDto.setSfx(10);
        ratingDto.setSound(9);
        ratingDto.setComment("comment");

        mockWebServer.enqueue(
            new MockResponse()
                .setBody(mapper.writeValueAsString(ratingDto))
                .addHeader("Content-Type", "application/json")
        );

        //when
        var response = ratingService.createRating(ratingDto);

        //then
        Assertions.assertEquals(
            "/rating",
            mockWebServer.takeRequest().getPath()
        );
        Assertions.assertEquals(ratingDto.getMovieId(), response.getMovieId());
        Assertions.assertEquals(ratingDto.getUserId(), response.getUserId());
        Assertions.assertEquals(ratingDto.getOverall(), response.getOverall());
        Assertions.assertEquals(ratingDto.getPlot(), response.getPlot());
        Assertions.assertEquals(ratingDto.getActing(), response.getActing());
        Assertions.assertEquals(ratingDto.getVisuals(), response.getVisuals());
        Assertions.assertEquals(ratingDto.getSfx(), response.getSfx());
        Assertions.assertEquals(ratingDto.getSound(), response.getSound());
        Assertions.assertEquals(ratingDto.getComment(), response.getComment());
    }
}
