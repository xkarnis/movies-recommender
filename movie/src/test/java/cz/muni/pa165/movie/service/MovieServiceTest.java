package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.movie.MockedEntities;
import cz.muni.pa165.movie.config.MovieTestConfiguration;
import cz.muni.pa165.movie.data.repository.MovieFilter;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@Import(MovieTestConfiguration.class)
@ActiveProfiles("test")
class MovieServiceTest {

    @Autowired
    MovieService service;

    @Test
    void findByIdExisting() {
        Assertions.assertSame(
            MockedEntities.PIRATES_OF_THE_CARIBBEAN,
            service.findById(MockedEntities.PIRATES_OF_THE_CARIBBEAN.getId())
        );
    }

    @Test
    void findByIdNotExisting() {
        Assertions.assertThrows(
            ResourceNotFoundException.class,
            () -> service.findById(2L)
        );
    }

    @Test
    void findAll() {
        Assertions.assertEquals(
            new PageImpl<>(MockedEntities.MOVIES),
            service.findAll(PageRequest.of(0, 1), null)
        );
    }

    @Test
    void findByTitleFilterExisting() {
        final MovieFilter filter = new MovieFilter();
        filter.setTitle(
            MockedEntities.PIRATES_OF_THE_CARIBBEAN.getTitle().substring(2, 8)
        );
        Assertions.assertEquals(
            new PageImpl<>(List.of(MockedEntities.PIRATES_OF_THE_CARIBBEAN)),
            service.findAll(PageRequest.of(0, 10), filter)
        );
    }

    @Test
    void findByTitleFilterNotExisting() {
        final MovieFilter filter = new MovieFilter();
        filter.setTitle("Not existing movie");
        Assertions.assertEquals(
            new PageImpl<>(List.of()),
            service.findAll(PageRequest.of(0, 10), filter)
        );
    }

    @Test
    void findByDirectorNameFilterExisting() {
        final MovieFilter filter = new MovieFilter();
        filter.setDirectorSurname(
            MockedEntities.GORE_VERBINSKI.getSurname().substring(2, 8)
        );
        Assertions.assertEquals(
            new PageImpl<>(
                List.of(
                    MockedEntities.PIRATES_OF_THE_CARIBBEAN,
                    MockedEntities.RANGO
                )
            ),
            service.findAll(PageRequest.of(0, 10), filter)
        );
    }

    @Test
    void findByDirectorNameFilterNotExisting() {
        final MovieFilter filter = new MovieFilter();
        filter.setDirectorSurname("Not existing director");
        Assertions.assertEquals(
            new PageImpl<>(List.of()),
            service.findAll(PageRequest.of(0, 10), filter)
        );
    }

    @Test
    void findByActorNameFilterExisting() {
        final MovieFilter filter = new MovieFilter();
        filter.setActorSurname(
            MockedEntities.JOHNNY_DEPP.getSurname().substring(2, 4)
        );
        Assertions.assertEquals(
            new PageImpl<>(
                List.of(
                    MockedEntities.PIRATES_OF_THE_CARIBBEAN,
                    MockedEntities.RANGO
                )
            ),
            service.findAll(PageRequest.of(0, 10), filter)
        );
    }

    @Test
    void findByActorNameFilterNotExisting() {
        final MovieFilter filter = new MovieFilter();
        filter.setActorSurname("Not existing actor");
        Assertions.assertEquals(
            new PageImpl<>(List.of()),
            service.findAll(PageRequest.of(0, 10), filter)
        );
    }

    @Test
    void create() {
        Assertions.assertSame(
            MockedEntities.PIRATES_OF_THE_CARIBBEAN,
            service.create(MockedEntities.PIRATES_OF_THE_CARIBBEAN)
        );
    }

    @Test
    void update() {
        Assertions.assertSame(
            MockedEntities.PIRATES_OF_THE_CARIBBEAN,
            service.update(MockedEntities.PIRATES_OF_THE_CARIBBEAN)
        );
    }

    @Test
    void updateNotExisting() {
        Assertions.assertThrows(
            ResourceNotFoundException.class,
            () -> service.update(MockedEntities.RANGO)
        );
    }

    @Test
    void delete() {
        service.delete(MockedEntities.PIRATES_OF_THE_CARIBBEAN);
    }
}
