package cz.muni.pa165.movie;

import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.model.Person;
import java.util.List;
import java.util.Set;

public class MockedEntities {

    public static final Person JOHNNY_DEPP;
    public static final Person ORLANDO_BLOOM;
    public static final Person KEIRA_KNIGHTLEY;
    public static final Person GORE_VERBINSKI;
    public static final Movie PIRATES_OF_THE_CARIBBEAN;
    public static final Movie RANGO;
    public static final Movie EMPTY_MOVIE;
    public static final List<Movie> MOVIES;
    public static final GenreDto ACTION;
    public static final GenreDto ADVENTURE;
    public static final GenreDto FANTASY;
    public static final List<GenreDto> GENRES;

    // Initialize genres
    static {
        ACTION = new GenreDto();
        ACTION.setId(1L);
        ACTION.setName("Action");

        ADVENTURE = new GenreDto();
        ADVENTURE.setId(2L);
        ADVENTURE.setName("Adventure");

        FANTASY = new GenreDto();
        FANTASY.setId(3L);
        FANTASY.setName("Fantasy");

        GENRES = List.of(ACTION, ADVENTURE, FANTASY);
    }

    // Initialize persons
    static {
        JOHNNY_DEPP = new Person();
        JOHNNY_DEPP.setName("Johnny");
        JOHNNY_DEPP.setSurname("Depp");

        ORLANDO_BLOOM = new Person();
        ORLANDO_BLOOM.setName("Orlando");
        ORLANDO_BLOOM.setSurname("Bloom");

        KEIRA_KNIGHTLEY = new Person();
        KEIRA_KNIGHTLEY.setName("Keira");
        KEIRA_KNIGHTLEY.setSurname("Knightley");

        GORE_VERBINSKI = new Person();
        GORE_VERBINSKI.setName("Gore");
        GORE_VERBINSKI.setSurname("Verbinski");
    }

    // Initialize PIRATES_OF_THE_CARIBBEAN
    static {
        final Set<Person> actors = Set.of(
            JOHNNY_DEPP,
            ORLANDO_BLOOM,
            KEIRA_KNIGHTLEY
        );
        final Set<Long> genres = Set.of(ACTION.getId(), FANTASY.getId());
        final Set<String> images = Set.of(
            "https://example.com/image1.jpg",
            "https://example.com/image2.jpg"
        );

        PIRATES_OF_THE_CARIBBEAN = new Movie();
        PIRATES_OF_THE_CARIBBEAN.setId(1L);
        PIRATES_OF_THE_CARIBBEAN.setTitle("Pirates of the Caribbean");
        PIRATES_OF_THE_CARIBBEAN.setDescription(
            "A pirate captain and his crew are in search of a legendary treasure."
        );
        PIRATES_OF_THE_CARIBBEAN.setActors(actors);
        PIRATES_OF_THE_CARIBBEAN.setDirector(GORE_VERBINSKI);
        PIRATES_OF_THE_CARIBBEAN.setGenreIds(genres);
        PIRATES_OF_THE_CARIBBEAN.setImages(images);

        GORE_VERBINSKI.getMoviesDirected().add(PIRATES_OF_THE_CARIBBEAN);
        actors.forEach(actor ->
            actor.getMoviesActedIn().add(PIRATES_OF_THE_CARIBBEAN)
        );
    }

    // Initialize RANGO
    static {
        final Set<Person> actors = Set.of(JOHNNY_DEPP);
        final Person director = GORE_VERBINSKI;
        final Set<Long> genres = Set.of(ADVENTURE.getId(), FANTASY.getId());
        final Set<String> images = Set.of("https://example.com/image3.jpg");

        RANGO = new Movie();
        RANGO.setId(2L);
        RANGO.setTitle("Rango");
        RANGO.setDescription(
            "A chameleon who struggles to find his place in a world of animals."
        );
        RANGO.setActors(actors);
        RANGO.setDirector(director);
        RANGO.setGenreIds(genres);
        RANGO.setImages(images);

        director.getMoviesDirected().add(RANGO);
        actors.forEach(actor -> actor.getMoviesActedIn().add(RANGO));
    }

    // Initialize EMPTY_MOVIE
    static {
        EMPTY_MOVIE = new Movie();
        EMPTY_MOVIE.setId(3L);
        EMPTY_MOVIE.setTitle("Empty movie");
    }

    // Initialize MOVIES
    static {
        MOVIES = List.of(PIRATES_OF_THE_CARIBBEAN, RANGO, EMPTY_MOVIE);
    }
}
