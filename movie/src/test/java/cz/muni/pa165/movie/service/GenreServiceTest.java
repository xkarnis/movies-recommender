package cz.muni.pa165.movie.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.core.api.genre.GenreDtoPageableResponse;
import cz.muni.pa165.movie.MockedEntities;
import java.io.IOException;
import java.util.Set;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class GenreServiceTest {

    public static MockWebServer mockBackEnd;
    public static GenreService genreService;
    private final ObjectMapper objectMapper;

    @Autowired
    GenreServiceTest(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();

        WebClientGenreService genreWebClientService = new WebClientGenreService(
            "http://localhost",
            Integer.toString(mockBackEnd.getPort()),
            "/api/v1/genres"
        );
        genreService = new GenreService(genreWebClientService);
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }

    @BeforeEach
    public void setGenreResponse() throws IOException {
        long genreCount = MockedEntities.GENRES.size();

        GenreDtoPageableResponse mockedPage = new GenreDtoPageableResponse(
            MockedEntities.GENRES,
            0,
            (int) genreCount,
            genreCount
        );

        mockBackEnd.enqueue(
            new MockResponse()
                .setBody(objectMapper.writeValueAsString(mockedPage))
                .addHeader("Content-Type", "application/json")
        );
    }

    @Test
    public void testGetGenresFromMovie() {
        Set<GenreDto> movieGenres = genreService.getGenresFromMovie(
            MockedEntities.PIRATES_OF_THE_CARIBBEAN
        );

        Assertions.assertEquals(
            Set.of(MockedEntities.ACTION, MockedEntities.FANTASY),
            movieGenres
        );
    }

    @Test
    public void testGetGenresFromMovieWithNoGenres() {
        Set<GenreDto> movieGenres = genreService.getGenresFromMovie(
            MockedEntities.EMPTY_MOVIE
        );
        Assertions.assertEquals(Set.of(), movieGenres);
    }
}
