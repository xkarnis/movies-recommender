# Movie microservice
Represents a microservice for managing movies. Provides a REST API that allows for CRUD operations on movies.

## Running the application
The microservice runs by default on `port 8080`. To run the microservice:
```shell
mvn clean install # compile the project first
mvn spring-boot:run
```
To run the microservice with custom port in case of port conflicts:
```shell
mvn spring-boot:run -Dspring-boot.run.arguments="--server.port=8002"
```

## Endpoints

### [GET] /swagger-ui/index.html
Swagger UI for the microservice. Open this in your browser to see the Open API documentation: <http://localhost:8002/swagger-ui/index.html>

### [GET] /api/v1/movies/{id}
Get movie with specific ID.
```shell
curl localhost:8002/api/v1/movies/1
```
Response:
```json
{
  "id": 1,
  "title": "Pirates of the Caribbean",
  "description": "A pirate captain and his crew are in search of a legendary treasure.",
  "director": {
    "id": 2,
    "name": "Gore",
    "surname": "Verbinski"
  },
  "actors": [
    {
      "id": 3,
      "name": "Johnny",
      "surname": "Depp"
    },
    {
      "id": 1,
      "name": "Orlando",
      "surname": "Bloom"
    },
    {
      "id": 4,
      "name": "Keira",
      "surname": "Knightley"
    }
  ],
  "genres": null,
  "images": null
}
```

### [GET] /api/v1/movies
Get all saved genres.
```shell
curl localhost:8002/api/v1/movies
```
Response:
```json
{
  "content": [
    {
      "id": 1,
      "title": "Pirates of the Caribbean",
      "description": "A pirate captain and his crew are in search of a legendary treasure."
    }
  ],
  "pageable": {
    "sort": {
      "empty": true,
      "sorted": false,
      "unsorted": true
    },
    "offset": 0,
    "pageNumber": 0,
    "pageSize": 20,
    "paged": true,
    "unpaged": false
  },
  "last": true,
  "totalPages": 1,
  "totalElements": 1,
  "size": 20,
  "number": 0,
  "sort": {
    "empty": true,
    "sorted": false,
    "unsorted": true
  },
  "first": true,
  "numberOfElements": 1,
  "empty": false
}
```
You can aditionally filter the list of movies by the movie title, director's surname, and actor's surname that played in the movie:
```shell
curl -v "localhost:8002/api/v1/movies?title=Carib&directorSurname=Verb&actorSurname=Kni"
```
Responds with page containing just the movie Pirates of the Caribbean, which has the director Gore Verbinski and actor Keira Knightley.
```json
[
  {
    "id": 1,
    "title": "Pirates of the Caribbean",
    "description": "A pirate captain and his crew are in search of a legendary treasure."
  }
]
```


### [POST] /api/v1/movies
Create a movie.
```shell
curl localhost:8002/api/v1/movies -X POST -H "Content-Type: application/json" -d '{"id": null, "title": "Doggie in Boots","description": "A doggie has only one life to spare."}'
```
Response:
```json
{
  "id": 2,
  "title": "Doggie in Boots",
  "description": "A doggie has only one life to spare.",
  "director": null,
  "actors": null,
  "genres": null,
  "images": null
}
```

### [PUT] /api/v1/movies/{id}
Update a movie with specific ID.
```shell
curl localhost:8002/api/v1/movies/2 -X PUT -H "Content-Type: application/json" -d '{"id":null,"title":"Puss in Boots"}'
```
Response:
```json
{
  "id": 2,
  "title": "Puss in Boots",
  "description": null,
  "director": null,
  "actors": null,
  "genres": null,
  "images": null
}
```

### [DELETE] /api/v1/movies/{id}
Delete a movie with specific ID.
```shell
curl localhost:8002/api/v1/movies/2 -X DELETE
```